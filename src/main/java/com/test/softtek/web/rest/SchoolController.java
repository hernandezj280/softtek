package com.test.softtek.web.rest;

import com.test.softtek.data.dtos.SchoolDto;
import com.test.softtek.data.dtos.ValidationErrorDto;
import com.test.softtek.data.dtos.ValidationErrorMessageDto;
import com.test.softtek.data.request.CreateSchoolRequest;
import com.test.softtek.data.response.ResponseHandler;
import com.test.softtek.web.services.ISchoolServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class SchoolController {

    @Autowired
    ISchoolServices service;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ValidationErrorDto validateFields(MethodArgumentNotValidException ex) {

        ValidationErrorDto validationErrorDto = new ValidationErrorDto();
        validationErrorDto.setError(HttpStatus.BAD_REQUEST.name());
        validationErrorDto.setStatus(HttpStatus.BAD_REQUEST.value());
        validationErrorDto.setTimestamp(new Date());
        List<ValidationErrorMessageDto> validationErrorMessage = new ArrayList<>();

        ex.getBindingResult().getFieldErrors().forEach(error -> {
            ValidationErrorMessageDto validation = new ValidationErrorMessageDto();
            validation.setField(error.getField());
            validation.setMessage(error.getDefaultMessage());
            validationErrorMessage.add(validation);
        });

        validationErrorDto.setValidationErrorMessage(validationErrorMessage);
        return validationErrorDto;
    }

    @RequestMapping(value = "/schools", method = RequestMethod.POST)
    public ResponseEntity<Object> createSchool(@Valid @RequestBody CreateSchoolRequest request)  throws Exception{
        SchoolDto schoolDto = service.createSchool(request);
        return ResponseHandler.responseEntityBuilder("School created successfully",HttpStatus.CREATED,schoolDto);
    }

    @RequestMapping(value = "/schools", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllSchools() throws Exception {
        List<SchoolDto> schoolDtoList = service.getAllSchool();
        return ResponseHandler.responseEntityBuilder("Response successful",HttpStatus.OK,schoolDtoList);
    }

}
