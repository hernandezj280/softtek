package com.test.softtek.data.dtos;

import lombok.Data;

@Data
public class ValidationErrorMessageDto {
    private String field;
    private String message;
}
