# Softtek 

## Getting started
    Softteck Code

## Start instructions
  - Clone repository
  - Load Maven dependencies.
  - Please change of maven version HOME PATH of IDE if you get an error :  org.apache.maven.model.validation.DefaultModelValidator: method 'void <init>()' not found
  
  for more information check
    https://youtrack.jetbrains.com/issue/IDEA-290419/Maven-3.8.5-NoSuchMethodError-org.apache.maven.model.validation.DefaultModelValidator-method-void-init-not-found

    for example go to.
      Intellj --> Preferences --> Build Tools --> Maven and there after selecte a maven version instead of wrapper and this will fix the error.
   - Run the proyect   
   - Please accept lombok dependency.
   - Make sure you have java 11 installed.

## Database
   - For the data persistence H2 in-memory databases was used.

## Unit Test.
  - Controller Layer.
  - Services Layer.

## Endpoint examples.
- Create School
  - Create example
    ```json
    POST http://127.0.0.1:8082/api/v1/schools 
    {
        "name" : "",
        "address" : "",
        "neighborhood" : "",
        "postalCode" : "",
        "city" : "",
        "phone" :

    }
    ```
    - Get a list of schools
    ```
      Get http://127.0.0.1:8082/api/v1/schools
    ```

## Name
Softeck Code.

## Description
APIRest code example.
