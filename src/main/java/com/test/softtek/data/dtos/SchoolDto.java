package com.test.softtek.data.dtos;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
public class SchoolDto {
    private Integer schoolId;
    private String name;
    private String address;
    private String neighborhood;
    private Integer postalCode;
    private String city;
    private String phone;
    private Boolean enabled;
    private Boolean deleted;
    private Date createdAt;
}
