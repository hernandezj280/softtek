package com.test.softtek.data.dtos;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ValidationErrorDto {
    private Date timestamp;
    private Integer status;
    private String error;
    private List<ValidationErrorMessageDto> validationErrorMessage;
}
