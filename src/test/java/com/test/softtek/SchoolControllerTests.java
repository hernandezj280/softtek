package com.test.softtek;

import com.google.gson.Gson;
import com.test.softtek.data.dtos.SchoolDto;
import com.test.softtek.data.persistence.entities.School;
import com.test.softtek.data.request.CreateSchoolRequest;
import com.test.softtek.web.rest.SchoolController;
import com.test.softtek.web.services.ISchoolServices;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.invocation.MatchersBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(SchoolController.class)
public class SchoolControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ISchoolServices services;

    private final String baseUrl = "/api/v1/schools";
    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());

    @Test
    void getSchool() throws Exception {

        SchoolDto school = new SchoolDto();
        school.setName("University of Arizona");
        school.setAddress("4035 S Riverpoint Pkwy");
        school.setNeighborhood("Riverpoint");
        school.setPostalCode(85040);
        school.setCity("Phoenix");
        school.setPhone("8449378679");
        school.setEnabled(true);
        school.setDeleted(false);
        school.setCreatedAt(new Date());

        SchoolDto secondSchool = new SchoolDto();
        secondSchool.setName("Arizona College - Mesa");
        secondSchool.setAddress("163 N Dobson Rd, AZ");
        secondSchool.setNeighborhood("Dobson");
        secondSchool.setPostalCode(85201);
        secondSchool.setCity("Mesa");
        secondSchool.setPhone("8557068382");
        secondSchool.setEnabled(true);
        secondSchool.setDeleted(false);
        secondSchool.setCreatedAt(new Date());

        List<SchoolDto> schoolList = new ArrayList<>();
        schoolList.add(school);
        schoolList.add(secondSchool);

        Mockito.when(services.getAllSchool()).thenReturn(schoolList);
        mockMvc.perform(get(baseUrl).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.size()", Matchers.is(2)))
                .andExpect(content().contentType(contentType));

        verify(services).getAllSchool();
    }

    @Test
    void createSchool() throws Exception {

        CreateSchoolRequest request = new CreateSchoolRequest();
        request.setName("University of Arizona");
        request.setAddress("4035 S Riverpoint Pkwy");
        request.setNeighborhood("Riverpoint");
        request.setPostalCode(85040);
        request.setCity("Phoenix");
        request.setPhone("8449378679");

        SchoolDto school = new SchoolDto();
        school.setName("University of Arizona");
        school.setAddress("4035 S Riverpoint Pkwy");
        school.setNeighborhood("Riverpoint");
        school.setPostalCode(85040);
        school.setCity("Phoenix");
        school.setPhone("8449378679");
        school.setEnabled(true);
        school.setDeleted(false);
        school.setCreatedAt(new Date());


        Mockito.when(services.createSchool(request)).thenReturn(school);

        mockMvc.perform(post(baseUrl).contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(request)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType));

        verify(services).createSchool(request);

    }
}
