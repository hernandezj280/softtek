package com.test.softtek.web.services;

import com.test.softtek.data.dtos.SchoolDto;
import com.test.softtek.data.persistence.entities.School;
import com.test.softtek.data.persistence.repository.SchoolRepository;
import com.test.softtek.data.request.CreateSchoolRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SchoolServicesBean implements ISchoolServices{

    @Autowired
    private SchoolRepository schoolRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public  SchoolDto createSchool(CreateSchoolRequest request)  {

        School row = schoolRepository.findByName(request.getName());
        if (row != null){
            throw new RuntimeException("School was previously registered" + " " + request.getName());
        }

        School school = new School();
        school.setName(request.getName());
        school.setAddress(request.getAddress());
        school.setNeighborhood(request.getNeighborhood());
        school.setPostalCode(request.getPostalCode());
        school.setCity(request.getCity());
        school.setPhone(request.getPhone());
        school.setEnabled(true);
        school.setDeleted(false);
        school.setCreatedAt(new Date());

        school = schoolRepository.save(school);
        SchoolDto schoolDto =  modelMapper.map(school, SchoolDto.class);
        System.out.println("Created : " + school);

        return schoolDto;
    }

    @Override
    public List<SchoolDto> getAllSchool() {
        List<School> schoolList = schoolRepository.findByEnabledTrueAndDeletedFalse();
        List<SchoolDto> schoolDtoList = schoolList.stream().map(school -> modelMapper.map(school, SchoolDto.class))
                .collect(Collectors.toList());
        return schoolDtoList;
    }

}
