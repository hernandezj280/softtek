package com.test.softtek.data.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

public class ResponseHandler {

    public static ResponseEntity<Object> responseEntityBuilder(
            String message,HttpStatus httpStatus,Object responseObject){
        Map<String,Object> response = new HashMap<>();
        response.put("message", message);
        response.put("data" , responseObject);
        response.put("status", httpStatus.value());
        response.put("timestamp" , new Date());
        return new ResponseEntity<>(response,httpStatus);
    }

}
