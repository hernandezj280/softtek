package com.test.softtek;

import com.test.softtek.data.dtos.SchoolDto;
import com.test.softtek.data.persistence.entities.School;
import com.test.softtek.data.persistence.repository.SchoolRepository;
import com.test.softtek.data.request.CreateSchoolRequest;
import com.test.softtek.web.services.SchoolServicesBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class SchoolServicesTests {

    @InjectMocks
    private SchoolServicesBean schoolServicesBean;

    @Mock
    private SchoolRepository schoolRepository;

    private School school;

    @BeforeEach
    public void ini(){

        MockitoAnnotations.initMocks(this);
        school = new School();
        school.setName("University of Arizona");
        school.setAddress("4035 S Riverpoint Pkwy");
        school.setNeighborhood("Riverpoint");
        school.setPostalCode(85040);
        school.setCity("Phoenix");
        school.setPhone("8449378679");
        school.setEnabled(true);
        school.setDeleted(false);
        school.setCreatedAt(new Date());
    }
    @Test
    public void createSchool(){
        CreateSchoolRequest request = new CreateSchoolRequest();
        request.setName("University of Arizona");
        request.setAddress("4035 S Riverpoint Pkwy");
        request.setNeighborhood("Riverpoint");
        request.setPostalCode(85040);
        request.setCity("Phoenix");
        request.setPhone("8449378679");
        Mockito.when(schoolRepository.save(any(School.class))).thenReturn(school).thenThrow(NullPointerException.class);
        final SchoolDto result = schoolServicesBean.createSchool(request);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getName(),school.getName());
    }
    @Test
    public void getAllSchool(){

        School secondSchool = new School();
        secondSchool.setName("Arizona College - Mesa");
        secondSchool.setAddress("163 N Dobson Rd, AZ");
        secondSchool.setNeighborhood("Dobson");
        secondSchool.setPostalCode(85201);
        secondSchool.setCity("Mesa");
        secondSchool.setPhone("8557068382");
        secondSchool.setEnabled(true);
        secondSchool.setDeleted(false);
        secondSchool.setCreatedAt(new Date());

        List<School> schoolList = new ArrayList<>();
        schoolList.add(school);
        schoolList.add(secondSchool);

        Mockito.when(schoolRepository.findByEnabledTrueAndDeletedFalse()).thenReturn(schoolList).thenThrow(NullPointerException.class);
        final List<SchoolDto> result = schoolServicesBean.getAllSchool();
        Assertions.assertNotNull(result);
        Assertions.assertEquals(2,result.size());

    }
}
