package com.test.softtek.data.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "School")
@Table(name = "schools")
public class School {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "school_id", nullable = false)
    private Integer schoolId;

    @Column(name = "name", nullable = false, length = 60)
    private String name;

    @Column(name = "address" , nullable = false, length = 200)
    private String address;

    @Column(name = "neighborhood", nullable = false, length = 50)
    private String neighborhood;

    @Column(name = "postal_code", nullable = true, length = 5)
    private Integer postalCode;

    @Column(name = "city", nullable = true, length = 50)
    private String city;

    @Column(name = "phone", nullable = true, length = 10)
    private String phone;

    @Column(name = "enabled", columnDefinition = "boolean default true")
    private Boolean enabled;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private Boolean deleted;

    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;


}
