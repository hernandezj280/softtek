package com.test.softtek.data.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CreateSchoolRequest {

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 60)
    private String name;

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 200)
    private String address;

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 50)
    private String neighborhood;

    private Integer postalCode;

    @Size(min = 1, max = 50)
    private String city;

    @Size(min = 1, max = 10)
    private String phone;

}
