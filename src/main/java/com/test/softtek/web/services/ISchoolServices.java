package com.test.softtek.web.services;

import com.test.softtek.data.dtos.SchoolDto;
import com.test.softtek.data.request.CreateSchoolRequest;
import java.util.List;

public interface ISchoolServices {
    SchoolDto createSchool(CreateSchoolRequest request);
    List<SchoolDto> getAllSchool();
}
