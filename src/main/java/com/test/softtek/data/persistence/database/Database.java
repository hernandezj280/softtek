package com.test.softtek.data.persistence.database;

import com.test.softtek.data.persistence.repository.SchoolRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Primary
@Component
public class Database  {

    @Autowired
    @Getter
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Getter
    private SchoolRepository schoolRepository;


}
